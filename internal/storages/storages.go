package storages

import (
	"gitlab.com/ptflp/go-rpc/internal/db/adapter"
	"gitlab.com/ptflp/go-rpc/internal/infrastructure/cache"
	vstorage "gitlab.com/ptflp/go-rpc/internal/modules/auth/storage"
	ustorage "gitlab.com/ptflp/go-rpc/internal/modules/user/storage"
)

type Storages struct {
	User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
