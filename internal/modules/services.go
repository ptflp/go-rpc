package modules

import (
	"gitlab.com/ptflp/go-rpc/internal/infrastructure/component"
	aservice "gitlab.com/ptflp/go-rpc/internal/modules/auth/service"
	uservice "gitlab.com/ptflp/go-rpc/internal/modules/user/service"
	"gitlab.com/ptflp/go-rpc/internal/storages"
)

type Services struct {
	User          uservice.Userer
	Auth          aservice.Auther
	UserClientRPC uservice.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
